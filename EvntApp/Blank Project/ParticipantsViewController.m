//
//  ParticipantsViewController.m
//  Blank Project
//
//  Created by Lee Silver on 3/17/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "ParticipantsViewController.h"

@interface ParticipantsViewController ()

@end

@implementation ParticipantsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //sliding menu
    if(![self.slidingViewController.underLeftViewController isKindOfClass:[menuViewController class]])
    {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];


    apd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    //notify this page to load more images
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMore) name:@"loadMore" object:nil];

    //load items (API call)
//    participants = [[NSMutableArray alloc] initWithArray:@[@"Yorkdale", @"Future Shop", @"Best Buy", @"Canadian Tire", @"Lowes", @"Loblaws", @"Bell", @"Rogers"]];

}
-(void)viewDidAppear:(BOOL)animated
{
    
    
    [apiCall getParticipants:^(NSArray *res) {
        NSLog(@"%@", res);
        apd.pplInRange = [[NSMutableArray alloc]initWithArray:res];
    } resultFailed:^(NSString *err) {
        //error
        NSLog(@"%@", err);
    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];

}

#pragma mark Table

-(void)loadMore
{
    [_tbl reloadInputViews];
    [_tbl reloadData];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return apd.pplInRange.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
            cell = [[SizableImageCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
    
    
    NSDictionary* item = [apd.pplInRange objectAtIndex:indexPath.row];
    
    [[cell imageView] setBackgroundColor:[UIColor darkGrayColor]];
    [[cell imageView] setContentMode:UIViewContentModeScaleAspectFit];
    [[cell textLabel] setText:[item objectForKey:@"name"]];
    @try {
        [cell imageView].layer.cornerRadius = [cell imageView].frame.size.height / 2;
        [cell imageView].layer.masksToBounds = YES;
        UIImage* userImg =[[apd feedImages]objectForKey:[item objectForKey:@"img"]];
//        NSString *string = [NSString stringWithFormat:@"%@", [item objectForKey:@"_id"]];
//        cell.detailTextLabel.text = string;
        
        if (userImg) {
            [[cell imageView]setImage:userImg];
        }
        else
        {
            [[cell imageView] setImage:nil];
            [cell.imageView setBackgroundColor:ourGray];
            UIActivityIndicatorView* loading = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 70, 70)];
            [loading setColor:[UIColor blackColor]];
            [loading startAnimating];
            [[cell imageView] addSubview:loading];
            
             [apd getImagesInBackground:[item objectForKey:@"img"]];
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    

    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    apd.currentPerson = [[NSMutableDictionary alloc]initWithDictionary:[apd.pplInRange objectAtIndex:indexPath.row]];
    
    [self performSegueWithIdentifier:@"gotoDetails" sender:self];
    
    return;
    
    NSDictionary* item = [apd.pplInRange objectAtIndex:indexPath.row];
    
    NSLog(@"%@", item);

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yy hh:mm:ss"];
    NSString *stringFromDate = [dateFormatter stringFromDate:[NSDate date]];
    NSLog(@"today : %@", stringFromDate);
    
    //NSString* poke = [NSString stringWithFormat:@"{\"type\": \"poke\”, \"date\":  \"29/05/14 17:22:42\”}"];

//    NSString* poke =[NSString stringWithFormat:@"{\"type\": \"poke\", \"_id\": \"%@\", \"pokeId\":\"%@\", \"date\": \"29/05/14 17:22:42\" }", @"538505332df8c56a9a201837", @"538ce4d62df8c5b1578f1f21"];
    NSString* poke =[NSString stringWithFormat:@"{\"type\": \"poke\", \"_id\": \"%@\", \"pokeId\":\"%@\", \"date\": \"%@\" }", [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"], [item objectForKey:@"_id"], stringFromDate];
    
    
    NSLog(@"%@", poke);
    [apd.ws sendText:poke];
    
}


@end
