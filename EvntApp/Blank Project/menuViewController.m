//
//  menuViewController.m
//
//
//  Created by Lee Silver on 1/29/14
//

#import "menuViewController.h"

@interface menuViewController ()

@end

@implementation menuViewController
@synthesize menu, tbl;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    apd = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    menu = [[NSMutableArray alloc]init];
    
    [self getMenuItems]; // doesn't work on background thread for some reason. Get back to this
    
    [self.slidingViewController setAnchorRightRevealAmount:270.0f];
    self.slidingViewController.underLeftWidthLayout = ECFullWidth;
    
   
   
    [tbl reloadData];
    NSURL *imageURL = [NSURL URLWithString:[apd.currentUser objectForKey:@"img"]];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    _img.image = [UIImage imageWithData:imageData];
    _img.layer.cornerRadius = _img.frame.size.width/2;
    _img.clipsToBounds = YES;
    
    [_img.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [_img.layer setBorderWidth: 1.0];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    _img.userInteractionEnabled = YES;
    [_img addGestureRecognizer:singleTap];
    
    _lblName.text = [apd.currentUser objectForKey:@"name"];
      
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(gotoVideo) name:@"gotoVideo" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(gotoPhoto) name:@"gotoPhoto" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(gotoFeed) name:@"gotoFeed" object:nil];

    

}

-(void)tapDetected{
    NSLog(@"single Tap on imageview");
    UIViewController* newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Profile"];
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    

    
}

-(void)gotoVideo
{
    UIViewController* newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"video"];
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];


    
}
-(void)gotoPhoto
{
    UIViewController* newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"photo"];
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
    
    
}
-(void)gotoFeed
{
    UIViewController* newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Feed"];
    CGRect frame = self.slidingViewController.topViewController.view.frame;
    self.slidingViewController.topViewController = newTopViewController;
    self.slidingViewController.topViewController.view.frame = frame;
    [self.slidingViewController resetTopView];
    
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getMenuItems
{
    //temp
    self.menu = [[NSMutableArray alloc] initWithArray:@[@"Feed", @"Profile", @"Crowd", @"Shopping Assistant", @"Settings"]];
    [self performSelectorOnMainThread:@selector(refreshTbl) withObject:nil waitUntilDone:YES];
    


}

-(void)refreshTbl
{
    NSLog(@"menu - %@", menu);
    
    [tbl reloadData];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {   
       // cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

    }

    
    [[cell textLabel] setTextColor:[UIColor whiteColor]];
    [cell setBackgroundColor:[UIColor darkGrayColor]];
    [[cell textLabel] setFont:[UIFont fontWithName:@"Comfortaa" size:16]];
    [[cell textLabel]setText:[menu objectAtIndex: indexPath.row]];

 
    NSLog(@"%@", [NSString stringWithFormat:@"%@.png", [cell textLabel].text]);
    [[cell imageView] setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png", [cell textLabel].text]]];
    
    return cell;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menu.count;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
  
    NSString *identifier = [NSString stringWithFormat:@"%@", [menu objectAtIndex:indexPath.row]];
    
    AppDelegate *apd =(AppDelegate *)[[UIApplication sharedApplication]delegate];
    apd.pageInfo = [menu objectAtIndex:indexPath.row];
    
    if ([identifier isEqualToString:@"sign out"]) {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
    }
    if ([identifier isEqualToString:@""]) {
        return;
    }
    if ([identifier isEqualToString:@"video"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoVideo" object:nil];

        
        return;

    }
    if ([identifier isEqualToString:@"photo"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoPhoto" object:nil];
        return;

    }
    UIViewController* newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    
        CGRect frame = self.slidingViewController.topViewController.view.frame;
        self.slidingViewController.topViewController = newTopViewController;
        self.slidingViewController.topViewController.view.frame = frame;
        [self.slidingViewController resetTopView];
    //}];
    [[tableView cellForRowAtIndexPath:indexPath]setSelected:NO];

    
}

@end
