//
//  ProfileDetailViewController.m
//  EVNTAPP
//
//  Created by Orin on 23/06/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "ProfileDetailViewController.h"
#import "ProfileViewController.h"

@interface ProfileDetailViewController ()

@end

@implementation ProfileDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"Detail view controller is here");

    _txtDescription.layer.cornerRadius = 10;
    
    apd = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    if ([apd.ProfileEditItem isEqualToString:@"Email"]) {
        //hide/show
        [_maleFemail setHidden:YES];
        [_bday setHidden:YES];
        [_txt setHidden:NO];
        //current value
        [_txt setText:[apd.currentUser objectForKey:@"email"]];
        //description
        [_txtDescription setText:@"Enter your new email address"];
        //Keyboard Type
        [_txt setKeyboardType:UIKeyboardTypeEmailAddress];
    }
    if ([apd.ProfileEditItem isEqualToString:@"Name"]) {
        //hide/show
        [_maleFemail setHidden:YES];
        [_bday setHidden:YES];
        [_txt setHidden:NO];
        //current value
        [_txt setText:[apd.currentUser objectForKey:@"name"]];
        //description
        [_txtDescription setText:@"Enter your name"];
        //Keyboard Type
        [_txt setKeyboardType:UIKeyboardAppearanceDefault];
    }
    if ([apd.ProfileEditItem isEqualToString:@"Gender"]) {
        //hide/show
        [_maleFemail setHidden:NO];
        [_bday setHidden:YES];
        [_txt setHidden:YES];
        //current value
        if ([[apd.currentUser objectForKey:@"gender"] isEqualToString:@"male"]) {
            [_maleFemail setSelectedSegmentIndex:0];
        }
        else
        {
            [_maleFemail setSelectedSegmentIndex:1];
        }
        [_txt setText:[apd.currentUser objectForKey:@"gender"]];
        //description
        [_txtDescription setText:@"Male or Female?"];
        //Keyboard Type
    }
    if ([apd.ProfileEditItem isEqualToString:@"Birth Date"]) {
        //hide/show
        [_maleFemail setHidden:YES];
        [_bday setHidden:NO];
        [_txt setHidden:YES];
        //current value
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd/MM/yyyy"];
        
        [_bday setDate:[dateFormat dateFromString:[apd.currentUser objectForKey:@"age"]]];
        //description
        [_txtDescription setText:@"Enter your date of birth"];
        //Keyboard Type
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
    }
    [[cell textLabel] setTextColor:[UIColor blackColor]];
    [[cell textLabel] setFont:[UIFont fontWithName:@"Comfortaa" size:16]];
  //  [[cell textLabel]setText:[_profileValues objectAtIndex: indexPath.row]];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (IBAction)btnReturn:(id)sender {

    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)tap:(id)sender {
    
    [_txt resignFirstResponder];
}
@end
