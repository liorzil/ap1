//
//  photoViewController.h
//  EVNTAPP
//
//  Created by Lee Silver on 2014-06-09.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "menuViewController.h"


@interface photoViewController : UIViewController
{
    AppDelegate *apd;
}
@property (strong, nonatomic) IBOutlet UIImageView *img;
- (IBAction)btnClose:(id)sender;
@end
