//
//  testViewController.m
//  EVNTAPP
//
//  Created by Lee Silver on 2014-05-21.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "testViewController.h"

@interface testViewController ()

@end

@implementation testViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    apd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnConnect:(id)sender {
    apd.connect;
}
- (IBAction)btnWend:(id)sender {
    
    
    [apd.ws sendText:[_txt text]];
    
}

- (IBAction)btnPing:(id)sender {
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoVideo" object:nil];

    
    //[apd.ws sendPing:[NSData data]];
}
- (IBAction)tap:(id)sender {
    [_txt resignFirstResponder];
}
@end
