//
//  ProfileViewController.h
//  Blank Project
//
//  Created by Lee Silver on 3/17/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "menuViewController.h"
#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>


@interface ProfileViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UIActionSheetDelegate>
{
    AppDelegate* apd;
    bool newMedia;

}
@property (strong, nonatomic) NSArray * profileMenu;
@property (strong, nonatomic) NSMutableArray *profileValues;
@property (strong, nonatomic) IBOutlet UIImageView *profileImage;

@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
- (IBAction)btnChangeImage:(id)sender;

- (IBAction)btnMenu:(id)sender;
@end
