//
//  feed.h
//  Blank Project
//
//  Created by Lee Silver on 3/10/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "feedCell.h"
#import "textFeedCellTableViewCell.h"
#import "menuViewController.h"
#import "AppDelegate.h"


@interface feed : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate* apd;
}
//@property (strong, nonatomic, )NSMutableArray* feed;

@property (strong, nonatomic) IBOutlet UILabel *lblTest;

@property (strong, nonatomic) IBOutlet UITableView *tbl;
- (IBAction)btnMenu:(id)sender;

@end
