//
//  AppDelegate.h
//
//  Created by Lee Silver on 2013-12-16.
//  Copyright (c) 2013 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Parse/Parse.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "WebSocket.h"
//#import <MediaPlayer/MediaPlayer.h>
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate,CBPeripheralManagerDelegate, WebSocketDelegate, UIAlertViewDelegate>

{
    NSArray* AFPages; //for sliding menu. The page that was clicked.
    NSString* imageToGet;
    
    //iBeacon
    CLLocationManager *_locationManager;
    NSUUID *_uuid;
    NSNumber *_power;
    CLBeaconRegion *region;
    CBPeripheralManager *_peripheralManager;
    NSMutableArray* regionsToMonitor;
    NSMutableDictionary* currentState;
    
    //sockets
    @private WebSocket* ws;
    NSMutableDictionary* beamInfo;
    
    //content
    NSURL* contentUrl;
    
}
@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, readonly) WebSocket* ws;

@property (strong,nonatomic) NSMutableDictionary* currentUser;


@property(strong, nonatomic)    NSString* pageInfo; //for sliding menu. The page that was clicked.
@property BOOL loggedIn; //just for testing until authentication is in place

@property (strong,nonatomic)NSMutableDictionary* feedImages;//Image data
@property (strong,nonatomic) NSMutableDictionary* currentFeedItem;
@property (strong,nonatomic) NSArray* beacons;
@property (strong,nonatomic) NSMutableDictionary* beamInfo;

@property (strong,nonatomic) NSMutableArray* feed;


//participants
@property (strong,nonatomic) NSMutableArray* pplInRange;
@property (strong,nonatomic) NSMutableDictionary* currentPerson;

@property (strong,nonatomic) NSString* ProfileEditItem;


-(void)getImagesInBackground:(NSString*)imageLocation;
-(void)connect;
-(void)sendLocalNotificationWithText:(NSString *)text;

- (UIImage *)cachedImage: (NSString*)url;

@end
