//
//  AppDelegate.m
//
//  Created by Lee Silver on 2013-12-16.
//  Copyright (c) 2013 Lee Silver. All rights reserved.
//

#import "AppDelegate.h"

#define wsAddress @"ws://69.172.254.167:8090"
//#define wsAddress @"ws://69.165.170.226:8090"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    
//    [Parse setApplicationId:@"EM8CiZiPFh7pSBA0c3Xq2TrGjG6mG6jafIv4T9SD"
//                  clientKey:@"92CM6nRMEoCRAe3vLAm3EBOANRtTDaCodcyDrQmg"];
    
    //[PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    [FBLoginView class];
    
    regionsToMonitor = [[NSMutableArray alloc]init];
    _pplInRange = [[NSMutableArray alloc]init];
    
    if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusAvailable) {
        
        NSLog(@"Background updates are available for the app.");
    }else if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied)
    {
        NSLog(@"The user explicitly disabled background behavior for this app or for the whole system.");
    }else if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted)
    {
        NSLog(@"Background updates are unavailable and the user cannot enable them again. For example, this status can occur when parental controls are in effect for the current user.");
    }
    [self startRanging];
    
//    [[NSUserDefaults standardUserDefaults]setObject:@"5372440f2df8c503961faf6b" forKey:@"userID"];
    
    _feed = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"feed"]];
    
    [self connect]; //for testing
    
    return YES;
}

//Store Device ID for easy access
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *deviceTokenString = [[[[NSString stringWithFormat:@"%@", deviceToken] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""];
    
    [[NSUserDefaults standardUserDefaults] setValue:deviceTokenString forKey:@"deviceID"];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSString *alertMsg; 
    NSString *badge;
    NSString *sound;
    
    int badgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeNumber+1];
    
    if( [[userInfo objectForKey:@"aps"] objectForKey:@"alert"] != NULL)
    {
        alertMsg = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    }
    else
    {    alertMsg = @"{no alert message in dictionary}";
    }
    
    if( [[userInfo objectForKey:@"aps"] objectForKey:@"badge"] != NULL)
    {
        badge = [[userInfo objectForKey:@"aps"] objectForKey:@"badge"];
    }
    else
    {    badge = @"{no badge number in dictionary}";
    }
    
    if( [[userInfo objectForKey:@"aps"] objectForKey:@"sound"] != NULL)
    {
        sound = [[userInfo objectForKey:@"aps"] objectForKey:@"sound"];
    }
    else
    {    sound = @"{no sound in dictionary}";
    }

}
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    
    return wasHandled;
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    if (_ws) {
        [_ws close];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (![CLLocationManager locationServicesEnabled]) {
        NSLog(@"Couldn't turn on ranging: Location services are not enabled.");
    }
    
    if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorized) {
        NSLog(@"Couldn't turn on monitoring: Location services not authorised.");
    }
}

-(BOOL)CanDeviceSupportAppBackgroundRefresh
{
    
    // Override point for customization after application launch.
    if ([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusAvailable) {
        NSLog(@"Background updates are available for the app.");
        return YES;
    }else if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusDenied)
    {
        NSLog(@"The user explicitly disabled background behavior for this app or for the whole system.");
        return NO;
    }else //if([[UIApplication sharedApplication] backgroundRefreshStatus] == UIBackgroundRefreshStatusRestricted)
    {
        NSLog(@"Background updates are unavailable and the user cannot enable them again. For example, this status can occur when parental controls are in effect for the current user.");
        return NO;
    }
}

#pragma mark Lazy Load in Background

- (UIImage *)cachedImage: (NSString*)url
{
    if (_feedImages == nil) {
        _feedImages = [[NSMutableDictionary alloc]init];
    }
    UIImage* theImage = [[UIImage alloc]init];
    theImage = [_feedImages objectForKey:url];
    if ((nil != theImage) && [theImage isKindOfClass:[UIImage class]]) {
        return theImage;
    }
    else {
        @try {
            NSLog(@"url: %@", url);
            NSLog(@"urlURL: %@", [NSURL URLWithString: url]);
            NSLog(@"nsdata: %@", [NSData dataWithContentsOfURL:[NSURL URLWithString: url]]);
            NSLog(@"uiImage: %@", [UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString: url]]]);
            theImage = [UIImage imageWithData: [NSData dataWithContentsOfURL:[NSURL URLWithString: url]]];
            [_feedImages setObject:theImage forKey:url];
        }
        @catch (NSException *exception) {
            theImage = NULL;
        }
        @finally {
            
        }
        return theImage;
    }
}

-(void)getImagesInBackground:(NSString*)imageLocation
{
    imageToGet = imageLocation;
    [self performSelectorInBackground:@selector(getImages) withObject:nil];
    
}
-(void)getImages
{
    
        @try {
            
            NSLog(@"image to get: %@", imageToGet);
            UIImage* img = [self cachedImage:imageToGet];
            
            if (img != nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadMore" object:self userInfo:nil];
                });
            }
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception.description);
        }
        @finally {
        }
}

//set beacon state and logs to DB if necessary
-(void)setBeaconState:(NSString*)state ForBeacon:(NSString*)beaconID
{
    
    if (currentState == nil) {
        currentState = [[NSMutableDictionary alloc]init];
    }
    
    NSString* beaconState = [currentState objectForKey:beaconID]; //In or Out
    
    if ([beaconState isEqualToString:state]) {
        return;
    }
    else
    {
        [currentState setObject:state forKey:beaconID];
        [apiCall logActivity:state forUser:@"lee" andBeacon:beaconID resultSuccess:^(NSDictionary *res) {
            NSLog(@"logging event: %@", res);
        } resultFailed:^(NSString *err) {
            NSLog(@"error logging activity: %@", err);
        }];
    }
    
    
}

#pragma mark - Beacon Range
-(void)startRanging{
    
    //Check if monitoring is available or not
    if (![CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Monitoring not available" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    if (_locationManager!=nil) {
        if(region){
            region.notifyOnEntry = YES;
            region.notifyOnExit = YES;
            region.notifyEntryStateOnDisplay = YES;
            [_locationManager startMonitoringForRegion:region];
            [_locationManager startRangingBeaconsInRegion:region];
            
        }
        else{
//            _uuid = [[NSUUID alloc] initWithUUIDString:@"E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"];
            _uuid = [[NSUUID alloc] initWithUUIDString:@"4F407DEA-0D3B-4E39-BFCF-FDB8E9A13CA9"];
//            _uuid = [[NSUUID alloc] initWithUUIDString:@"80EB3DF6-AE4C-4B7F-8157-12C9AB6C962E"];

            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            region = [[CLBeaconRegion alloc] initWithProximityUUID:_uuid identifier:@"com.ap1ication.beacon"];
            if(region){
                region.notifyOnEntry = YES;
                region.notifyOnExit = YES;
                region.notifyEntryStateOnDisplay = YES;
                [_locationManager startMonitoringForRegion:region];
                [_locationManager startRangingBeaconsInRegion:region];
                
            }
        }
    }
    else{
//        _uuid = [[NSUUID alloc] initWithUUIDString:@"E2C56DB5-DFFB-48D2-B060-D0F5A71096E0"];
        _uuid = [[NSUUID alloc] initWithUUIDString:@"4F407DEA-0D3B-4E39-BFCF-FDB8E9A13CA9"];
//        _uuid = [[NSUUID alloc] initWithUUIDString:@"80EB3DF6-AE4C-4B7F-8157-12C9AB6C962E"];
        
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        
        
        region = [[CLBeaconRegion alloc] initWithProximityUUID:_uuid identifier:@"com.ap1ication.beacon"];
        if(region){
            region.notifyOnEntry = YES;
            region.notifyOnExit = YES;
            region.notifyEntryStateOnDisplay = YES;
            [_locationManager startMonitoringForRegion:region];
            [_locationManager startRangingBeaconsInRegion:region];
            
        }
    }
}



-(void)stopRanging{
    [_locationManager stopRangingBeaconsInRegion:region];
    [_locationManager stopMonitoringForRegion:region];
}


#pragma mark - Location manager beacon region delegate

-(void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region{
    NSLog(@"Enter Region  %@",region);
    [_locationManager startRangingBeaconsInRegion:region];
   // [self sendLocalNotificationForReqgionConfirmationWithText:@"REGION INSIDE"];
   // [self setBeaconState:@"In" ForBeacon:region.identifier];
    
    [self connect];
    
//    if (!_ws == NULL) {
//        [self initWS];
//    }
}

-(void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region{
    NSLog(@"Exit Region  %@",region);
  //  [self sendLocalNotificationForReqgionConfirmationWithText:@"REGION OUTSIDE"];
    [_locationManager stopRangingBeaconsInRegion:region];
    
    [ws close];
    
    //[self setBeaconState:@"Out" ForBeacon:region.identifier];
}

-(void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region{
    NSLog(@"Monitoring for %@",region);
   // [self sendLocalNotificationForReqgionConfirmationWithText:@"MONITORING STARTED"];
    
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region{

   // UILocalNotification *notification = [[UILocalNotification alloc] init];

    
    if (state == CLRegionStateInside) {
        [_locationManager startRangingBeaconsInRegion:region];
        //[self sendLocalNotificationForReqgionConfirmationWithText:@"REGION INSIDE"];
       // [self setBeaconState:@"In" ForBeacon:region.identifier];
        //notification.alertBody = [NSString stringWithFormat:@"You are inside the region %@", region.identifier];
//        if (!_ws) {
//            [self initWS];
//        }
        [self connect];
    }
    else{
        //[[BluetoothManager shared] scan];
      //  [self sendLocalNotificationForReqgionConfirmationWithText:@"REGION OUTSIDE"];
        [_locationManager stopRangingBeaconsInRegion:region];
       // [self setBeaconState:@"Out" ForBeacon:region.identifier];
       // notification.alertBody = [NSString stringWithFormat:@"You are outside the region %@", region.identifier];
        [_ws close];
    }
    //[_locationManager startRangingBeaconsInRegion:region];
    //[[UIApplication sharedApplication] presentLocalNotificationNow:notification];

}

- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral{
    NSLog(@"peripheral %@",peripheral);
    
}
-(void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(CLBeaconRegion *)region
{
    
    NSArray *unknownBeacons = [beacons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"proximity = %d", CLProximityUnknown]];
    if([unknownBeacons count]){
        NSLog(@"unknown beacons %@",unknownBeacons);
        //[_output setText:[NSString stringWithFormat:@"unknown beacons %@",unknownBeacons]];
        //DB log
    }
    
    NSArray *immediateBeacons = [beacons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"proximity = %d", CLProximityImmediate]];
    if([immediateBeacons count]){
        NSLog(@"immediate beacons %@",immediateBeacons);
        //[_output setText:[NSString stringWithFormat:@"immediate beacons %@",immediateBeacons]];
        //DB log
    }
    
    
    NSArray *nearBeacons = [beacons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"proximity = %d", CLProximityNear]];
    if([nearBeacons count]){
        NSLog(@"near beacons %@",nearBeacons);
        //[_output setText:[NSString stringWithFormat:@"near beacons %@",nearBeacons]];
       //DB log
        
    }
    NSArray *farBeacons = [beacons filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"proximity = %d", CLProximityFar]];
    if([farBeacons count]){
        NSLog(@"far beacons %@",farBeacons);
        //DB log
    }
}
-(void)sendLocalNotificationWithText:(NSString *)text{

    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    if (localNotif == nil)
        return;
    
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    localNotif.alertBody = [NSString stringWithFormat:NSLocalizedString(@"%@", nil),
                            text];
    localNotif.alertAction = NSLocalizedString(@"View Details", nil);
    
    localNotif.applicationIconBadgeNumber = localNotif.applicationIconBadgeNumber + 1;
    
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:text forKey:@"KEY"];
    localNotif.userInfo = infoDict;
    
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotif];
    
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification NS_AVAILABLE_IOS(4_0){
    UIAlertView *alert  = [[UIAlertView alloc] initWithTitle:notification.alertBody message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}


#pragma mark Web Socket
/**
 * Called when the web socket connects and is ready for reading and writing.
 **/
- (void) didOpen
{
    NSLog(@"Socket is open.");
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yy hh:mm:ss"];
    NSString *stringFromDate = [dateFormatter stringFromDate:[NSDate date]];

    NSLog(@"{ \"type\": \"config\", \"status\": \"Entered\", \"text\": \"ID Value\", \"_id\": \"%@\", \"date\": \"%@\" }", [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"], stringFromDate);
                                                                                                                            
 [_ws sendText:[NSString stringWithFormat:@"{ \"type\": \"config\", \"status\": \"Entered\", \"text\": \"ID Value\", \"_id\": \"%@\", \"date\": \"%@\" }", [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"], stringFromDate]];
    
}

/**
 * Called when the web socket closes. aError will be nil if it closes cleanly.
 **/
- (void) didClose:(NSUInteger) aStatusCode message:(NSString*) aMessage error:(NSError*) aError
{
    NSLog(@"Socket closed.");
}

/**
 * Called when the web socket receives an error. Such an error can result in the
 socket being closed.
 **/
- (void) didReceiveError:(NSError*) aError
{
    NSLog(@"Error occurred.");

}

/**
 * Called when the web socket receives a message.
 **/
- (void) didReceiveTextMessage:(NSString*) aMessage
{
    NSLog(@"Did receive message: %@", aMessage);
    
    NSError *e;
    beamInfo = [[NSMutableDictionary alloc]init];
    beamInfo =
    [NSJSONSerialization JSONObjectWithData: [aMessage dataUsingEncoding:NSUTF8StringEncoding]
                                    options: NSJSONReadingMutableContainers
                                      error: &e];
    
    
    NSLog(@"Beam: %@", beamInfo);
    
    if ([[beamInfo objectForKey:@"msgT"] isEqualToString:@"message"]) {
        [_feed insertObject:beamInfo atIndex:0];
        
        [[NSUserDefaults standardUserDefaults] setObject:_feed forKey:@"feed"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loadMore" object:self userInfo:nil];
        

    }
    else if([[beamInfo objectForKey:@"msgT"] isEqualToString:@"poke"])
    {
        
        [self sendLocalNotificationWithText:[NSString stringWithFormat:@"Ping received from %@", [beamInfo objectForKey:@"name"]]];
    }
    else
    {
        
    }
    

    
    
//    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:[beamInfo objectForKey:@"title"] message:@"Message from EVNTAP" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:@"Open", nil];
//    [alert setTag:8343];
//    [alert show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag]==8343) {
        if ([alertView cancelButtonIndex] != buttonIndex) {
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"gotoFeed" object:self userInfo:nil];

            
            //open image, video, text, etc.
//            NSLog(@"opened");
//            
//            NSLog(@"%@", [[beamInfo objectForKey:@"elements"] class]);
//            
//            NSLog(@"%@", [beamInfo objectForKey:@"elements"]);
//
//            NSLog(@"%@", [((NSDictionary*)[beamInfo objectForKey:@"elements"])objectForKey:@"content"]);
//        
//            NSDictionary* elements = [[NSDictionary alloc]init];
//            elements = [beamInfo objectForKey:@"elements"];
//            NSLog(@"%@, Type = %@, content = %@", elements, [elements objectForKey:@"type"], [elements objectForKey:@"content"]);
            
            //Video
//            if ([[elements objectForKey:@"type"] isEqualToString:@"video"]) {
//                //contentUrl=[[NSURL alloc] initWithString:[elements objectForKey:@"content"]];
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotoVideo" object:self userInfo:nil];
//
//
//                
//            }
            
        }
    }
}

/**
 * Called when the web socket receives a message.
 **/
- (void) didReceiveBinaryMessage:(NSData*) aMessage
{
    //Hooray! I got a binary message.
    NSLog(@"Did receive binary message: %@", aMessage);

}

/**
 * Called when pong is sent... For keep-alive optimization.
 **/
- (void) didSendPong:(NSData*) aMessage
{
    NSLog(@"Pong was sent!");
}


- (void)initWS
{
        //make sure to use the right url, it must point to your specific web socket endpoint or the handshake will fail
        //create a connect config and set all our info here
        WebSocketConnectConfig* config = [WebSocketConnectConfig configWithURLString:wsAddress origin:nil protocols:nil tlsSettings:nil headers:nil verifySecurityKey:YES extensions:nil ];
        config.closeTimeout = 15.0;
        config.keepAlive = 15.0; //sends a ws ping every 15s to keep socket alive
        
        //setup dispatch queue for delegate logic (not required, the websocket will create its own if not supplied)
        dispatch_queue_t delegateQueue = dispatch_queue_create("myWebSocketQueue", NULL);
        
        //open using the connect config, it will be populated with server info, such as selected protocol/etc
        _ws = [WebSocket webSocketWithConfig:config queue:delegateQueue delegate:self];
         [self.ws open];
}
//
////testing
-(void)connect
{
    if (!_ws) {
        [self initWS];
//        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"" message:@"connected" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        [alert show];
    }
    else
    {
        _ws = nil;
        [self initWS];
    }
}
@end
