//
//  modelClass.m
//
//  Created by Lee Silver on 2013-10-24.
//  Copyright (c) 2013 Lee Silver. All rights reserved.
//
#import "modelClass.h"
    
static ModelClass *modelClass = nil;

@implementation ModelClass

+ (ModelClass *) sharedModel {
    @synchronized(self)
    {
        if(modelClass == nil) {
            modelClass = [[ModelClass alloc] init];
        }
    }
    return modelClass;
}

#pragma mark keychain

///Saves token in keychain
-(void)saveToken:(id)accessToken
{
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"authToken" accessGroup:nil];
    
    [keychainItem setObject:accessToken forKey:(__bridge id)(kSecValueData)];
    
}

///retreives token from keychain
- (id)getToken{

    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"authToken" accessGroup:nil];

    return [keychainItem objectForKey:(__bridge id)(kSecValueData)];
}

#pragma mark validation
//Checks if string is email format
- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    
    //the following might be better
    /*    NSString *emailRegex =
     @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
     @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
     @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
     @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
     @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
     @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
     @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";*/
    
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

//checks if string is phone format
-(BOOL) validatePhone:(NSString*)phoneNumber
{
    NSString *phoneRegex = @"^[\\+(00)][0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
    
    
}

//checks if string is numeric
-(BOOL)isNumeric:(NSString*)text
{
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([text rangeOfCharacterFromSet:notDigits].location == NSNotFound)
    {
        return true;
    }
    else
        return false;
}

//returns readable date from UTC
-(NSString*)niceDate:(NSString*)dateToFormat
{
    NSDate* theDate = [[NSDate alloc]init];
    
    NSLog(@"%@", theDate);
    NSLog(@"%@", dateToFormat);
    NSDateFormatter *formatDate = [[NSDateFormatter alloc]init];
    
    [formatDate setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *tz = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
    [formatDate setTimeZone:tz];
    NSString *dateString ;
    
    theDate =[formatDate dateFromString:dateToFormat];
    
    NSLog(@"%@", theDate);
    
    [formatDate setDateFormat:@"MMM dd h:mm a"];
    tz = [NSTimeZone systemTimeZone];
    [formatDate setTimeZone:tz];
    
    dateString = [formatDate stringFromDate:theDate];
    
    return dateString;
}

#pragma mark API Calls
//Send a POST request with parameters in the URL
-(void)sendRequestWithMethod:(NSString*)method AndParameters:(NSDictionary*)params resultSuccess:(void (^)(NSArray *))success resultFailed:(void (^)(NSString *))failed
{
    //create string of parameters and values taken from NSDictionary (3=Three&1=one&4=Four&2=Two)
    NSString *s = [NSString stringWithFormat:@"&API_KEY=%@", apiKey];
    
    for (int i = 0; i < params.allKeys.count; i++) {
        s = [s stringByAppendingFormat:@"&%@=%@",(NSString*)[params.allKeys objectAtIndex:i], (NSString*)[params objectForKey:[params.allKeys objectAtIndex:i]]];
    }
    
    NSString* post = [s stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", [NSString stringWithFormat:@"%@%@", api_domain, method]);
    
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", api_domain, method]]]; //api_domain
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *err;
    
    NSData *jsonData = [ NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSError *error = nil;
    
    if (jsonData) {
        
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        
        if (error) {
            NSLog(@"error is %@", [error localizedDescription]);
            //            failed = [NSString stringWithFormat:@"error is %@", [error localizedDescription]];
            failed([NSString stringWithFormat:@"error is %@", [error localizedDescription]]);
            return;
            
        }
        success((NSArray*)jsonObjects);
    }
    
    
}

//Send a POST request with JSON parameters to receive a key value pair of data
-(void)sendRequestWithMethod:(NSString*)method AndJSONParameters:(NSDictionary*)params resultSuccess:(void(^)(NSDictionary *))success resultFailed:(void(^)(NSString*))failed
{
    NSString* post = [[NSString alloc]init];
    
   // post = [NSString stringWithFormat:@"&API_KEY=%@", apiKey];
     post = @"";
    
    NSError* error = nil;
    NSLog(@"%@", params);
    NSData *jsonParameters = [NSJSONSerialization dataWithJSONObject:params
                                                             options:NSJSONWritingPrettyPrinted
                                                               error:&error];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", api_domain, method, post]]]; //api_domain
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonParameters length]] forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPBody:jsonParameters];
    
    NSURLResponse *response;
    NSError *err;
    
    NSData *jsonData = [ NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    
    
    if (jsonData) {
        
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        
        
        
        if (error) {
            NSLog(@"error is %@", [error localizedDescription]);
            //            failed = [NSString stringWithFormat:@"error is %@", [error localizedDescription]];
            failed([NSString stringWithFormat:@"error is %@", [error localizedDescription]]);
            return;
            
        }
        success(jsonObjects);
    }
    
    
}

//get a list
-(void)getArrayBySendingRequestWithMethod:(NSString*)method AndJSONParameters:(NSDictionary*)params resultSuccess:(void(^)(NSArray *))success resultFailed:(void(^)(NSString*))failed
{
    NSString* post = [[NSString alloc]init];
    
    post = [NSString stringWithFormat:@"API_KEY=%@", apiKey];
    
    
    NSError* error = nil;
    NSLog(@"%@", params);
    NSData *jsonParameters = [NSJSONSerialization dataWithJSONObject:params
                                                             options:NSJSONWritingPrettyPrinted
                                                               error:&error];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", api_domain, method, post]]];
    
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[jsonParameters length]] forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPBody:jsonParameters];
    
    NSURLResponse *response;
    NSError *err;
    
    NSData *jsonData = [ NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    NSLog(@"%@", response.description);
    
    if (jsonData) {
        
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        
        
        
        if (error) {
            NSLog(@"error is %@", [error localizedDescription]);
            //            failed = [NSString stringWithFormat:@"error is %@", [error localizedDescription]];
            failed([NSString stringWithFormat:@"error is %@", [error localizedDescription]]);
            return;
            
        }
        success(jsonObjects);
    }
    
    
}

//send 'GET' request
-(void)sendGETRequestWithMethod:(NSString*)method resultSuccess:(void(^)(NSArray *))success resultFailed:(void(^)(NSString*))failed
{
    NSString* post = [[NSString alloc]init];
    post = [NSString stringWithFormat:@"API_KEY=%@", apiKey];

    
    NSError* error = nil;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", api_domain, method, post]]]; //api_domain
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response;
    NSError *err;
    
    NSData *jsonData = [ NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];

    if (jsonData) {
        
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        
        if (error) {
            NSLog(@"error is %@", [error localizedDescription]);
            
            failed([NSString stringWithFormat:@"error is %@", [error localizedDescription]]);
            return;
            
        }
        success((NSArray*)jsonObjects);
    }
    
    
}

#pragma mark Parse Functions

//-(void)callParseCloudMethodInBackground:(NSString*)methodName andParameters:(NSDictionary*)parameters resultSuccess:(void(^)(NSDictionary *))success resultFailed:(void(^)(NSString*))failed
//{
//    [PFCloud callFunctionInBackground:methodName withParameters:parameters block:^(NSDictionary *result, NSError *error) {
//    
//        NSLog(@"%@", result);
//            if (!error) {
//                success(result);
//            }
//            else
//            {
//                failed(error.description);
//            }
//    }];
//    
//    
//}
//
//
//-(void)callParseCloudMethod:(NSString*)methodName andParameters:(NSDictionary*)parameters
//{
//    [PFCloud callFunction:methodName withParameters:parameters];
//    
//}

#pragma mark miscellaneous

//set a notification to pop up once at a specific date/time. set the title and the button text
-(void)setNonRepeatingNotificationForDate:(NSDate*)notificationDate andTitle:(NSString*)title andButtonText:(NSString*)buttonText
{
    UILocalNotification *localNotify=[[UILocalNotification alloc]init];
    
    localNotify.fireDate = notificationDate;
    localNotify.alertBody=title;
    localNotify.applicationIconBadgeNumber=1;
    localNotify.soundName=UILocalNotificationDefaultSoundName;
    localNotify.alertAction=buttonText;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotify];
}

//set a notification to pop up repeatedly. set the date, title, the button text and the repeat interval
//repeat interval:  NSWeekCalendarUnit; NSWeekdayCalendarUnit; NSWeekdayOrdinalCalendarUnit; NSWeekOfMonthCalendarUnit; NSWeekOfYearCalendarUnit;
-(void)setRepeatingNotificationForDate:(NSDate*)notificationDate andTitle:(NSString*)title andButtonText:(NSString*)buttonText andRepeatInterval:(NSCalendarUnit)repeatInterval
{
    UILocalNotification *localNotify=[[UILocalNotification alloc]init];
 
    localNotify.fireDate = notificationDate;
    localNotify.repeatInterval = repeatInterval;
    localNotify.alertBody=title;
    localNotify.applicationIconBadgeNumber=1;
    localNotify.soundName=UILocalNotificationDefaultSoundName;
    localNotify.alertAction=buttonText;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotify];
}

@end
