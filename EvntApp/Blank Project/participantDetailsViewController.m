//
//  participantDetailsViewController.m
//  Blank Project
//
//  Created by Lee Silver on 3/17/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "participantDetailsViewController.h"

@interface participantDetailsViewController ()

@end

@implementation participantDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    apd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    _img.layer.cornerRadius = _img.frame.size.width/2;
    _img.clipsToBounds =YES;
    UIImage* userImg =[[apd feedImages]objectForKey:[apd.currentPerson objectForKey:@"img"]];
    [_img setImage:userImg];
    [_lblCompanyName setText:[apd.currentPerson objectForKey:@"name"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Table

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
    
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return @"About Us";
            break;
        case 1:
            return @"Contact";
            break;
            
        default:
            return @"";
            break;
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    [[cell textLabel] setNumberOfLines:0];
    
    switch (indexPath.section) {
        case 0:
        {
            [[cell textLabel]setText:@"This is a paragraph about this company. "];

            break;
        }
        case 1:
            [[cell textLabel]setText:@"This is the company's contact information"];
            break;
            
        default:
            [[cell textLabel]setText:@""];

            break;
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (IBAction)btnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)ping:(id)sender {
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yy hh:mm:ss"];
    NSString *stringFromDate = [dateFormatter stringFromDate:[NSDate date]];
    NSLog(@"today : %@", stringFromDate);
    
    NSString* poke =[NSString stringWithFormat:@"{\"type\": \"poke\", \"_id\": \"%@\", \"pokeId\":\"%@\", \"date\": \"%@\" }", [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"], [apd.currentPerson objectForKey:@"_id"], stringFromDate];
    
    
    NSLog(@"%@", poke);
    [apd.ws sendText:poke];
    
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Ping Sent" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];

}
@end
