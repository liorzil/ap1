//
//  apiCall.m
//  EVNTAPP
//
//  Created by Lee Silver on 2014-03-26.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "apiCall.h"

@implementation apiCall

-(void)ExampleFunctionToCallAPI:(NSString*)input1 andSecondInput:(NSString*)input2 resultSuccess:(void(^)(NSDictionary *res))success resultFailed:(void(^)(NSString *err))failed{
    
    //Create NSDictionary Params
    NSDictionary* params = [[NSDictionary alloc]initWithObjectsAndKeys:
                            input1,@"key1",
                            input2,@"key2",nil];
    //call generic api method parameters
    [mc sendRequestWithMethod:@"WebServiceMethodName" AndJSONParameters:params resultSuccess:^(NSDictionary *results) {
        //can do more validation here and return success or fail
        success(results);
    } resultFailed:^(NSString *err) {
        failed(err);
    }];
    
    
}
+(void)loginWithParameters:(NSDictionary*)params resultSuccess:(void(^)(NSDictionary *res))success resultFailed:(void(^)(NSString *err))failed{
    
    [mc sendRequestWithMethod:@"login" AndJSONParameters:params resultSuccess:^(NSDictionary *results) {
        success(results);

    } resultFailed:^(NSString *err) {
        failed(err);

    }];
}
+(void)getFeed:(void(^)(NSArray *res))success resultFailed:(void(^)(NSString *err))failed{
    
    
    ///////////temp. Only because the new server is not available yet
        
    NSError* error = nil;
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    
    [request setURL:[NSURL URLWithString: @"http://evntbeam.apiary-mock.com/beam_feed"]]; //api_domain
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response;
    NSError *err;
    
    NSData *jsonData = [ NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
    
    if (jsonData) {
        
        
        id jsonObjects = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
        
        if (error) {
            NSLog(@"error is %@", [error localizedDescription]);
            
            failed([NSString stringWithFormat:@"error is %@", [error localizedDescription]]);
            return;
            
        }
        success((NSArray*)jsonObjects);
    }

    return;
    [mc sendGETRequestWithMethod:@"feed" resultSuccess:^(NSArray *results) {
        success(results);
    } resultFailed:^(NSString *err) {
        failed(err);
    }];
    
    
    return;
    NSDictionary* params = [[NSDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"userID"],@"userID", nil];
    
    [mc getArrayBySendingRequestWithMethod:@"getFeed" AndJSONParameters:params resultSuccess:^(NSArray *results) {
        NSLog(@"%@", results);
        success((NSArray*)results);
    } resultFailed:^(NSString *err) {
        NSLog(@"%@", err);
        failed(err);
    }];
    
    //[mc callParseCloudMethodInBackground:@"getFeed" andParameters:@{} resultSuccess:^(NSDictionary *results) {
        
        
//        id jsonObjects = [NSJSONSerialization JSONObjectWithData:results options:NSJSONReadingMutableContainers error:&error];
        
//        NSLog(@"%@", results);
//        success((NSArray*)results);
//    } resultFailed:^(NSString *err) {
//        NSLog(@"%@", err);
//        failed(err);
//    }];
    
    
}
+(void)logActivity:(NSString*)activity forUser:(NSString*)username andBeacon:(NSString*)beaconID resultSuccess:(void(^)(NSDictionary *res))success resultFailed:(void(^)(NSString *err))failed{
    
    //Create NSDictionary Params
    NSDictionary* params = [[NSDictionary alloc]initWithObjectsAndKeys:
                            activity, @"activity",
                            username,@"username",
                            beaconID,@"beaconIdentifier",nil];
    
    [mc sendRequestWithMethod:@"activityLog" AndJSONParameters:params resultSuccess:^(NSDictionary *results) {
        NSLog(@"%@", results);
        success(results);
    } resultFailed:^(NSString *err) {
        failed(err);
    }];
    
//    [mc callParseCloudMethodInBackground:@"activityLog" andParameters:params resultSuccess:^(NSDictionary *results) {
//        
//        //        id jsonObjects = [NSJSONSerialization JSONObjectWithData:results options:NSJSONReadingMutableContainers error:&error];
//        
//        NSLog(@"%@", results);
//        success(results);
//    } resultFailed:^(NSString *err) {
//        NSLog(@"%@", err);
//        failed(err);
//    }];
    
    
}
+(void)getBeacons:(void(^)(NSArray *res))success resultFailed:(void(^)(NSString *err))failed{
    
//    NSDictionary* params = [[NSDictionary alloc]initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults]objectForKey:@"userID"],@"userID", nil];
    
    [mc sendGETRequestWithMethod:@"beacon" resultSuccess:^(NSArray *results) {
        success((NSArray*)results);

    } resultFailed:^(NSString *err) {
        failed(err);

    }];
    
}
+(void)getParticipants:(void(^)(NSArray *res))success resultFailed:(void(^)(NSString *err))failed{
    
    [mc sendGETRequestWithMethod:@"onlineGuest" resultSuccess:^(NSArray *results) {
        success((NSArray*)results);
        
    } resultFailed:^(NSString *err) {
        failed(err);
        
    }];
    
}
+(void)setPrivacy:(NSNumber*)allowPing forUser:(NSString*)username resultSuccess:(void(^)(NSDictionary *res))success resultFailed:(void(^)(NSString *err))failed{
    
    //Create NSDictionary Params
    NSDictionary* params = [[NSDictionary alloc]initWithObjectsAndKeys:
                            allowPing, @"user_privacy",
                            username,@"user_id",nil];

    [mc sendRequestWithMethod:@"togglePrivacy" AndJSONParameters:params resultSuccess:^(NSDictionary *results) {
        NSLog(@"%@", results);
        success(results);
    } resultFailed:^(NSString *err) {
        failed(err);
    }];
    
    //    [mc callParseCloudMethodInBackground:@"activityLog" andParameters:params resultSuccess:^(NSDictionary *results) {
    //
    //        //        id jsonObjects = [NSJSONSerialization JSONObjectWithData:results options:NSJSONReadingMutableContainers error:&error];
    //
    //        NSLog(@"%@", results);
    //        success(results);
    //    } resultFailed:^(NSString *err) {
    //        NSLog(@"%@", err);
    //        failed(err);
    //    }];
    
    
}@end
