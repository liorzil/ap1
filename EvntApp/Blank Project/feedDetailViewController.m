//
//  feedDetailViewController.m
//  Blank Project
//
//  Created by Lee Silver on 3/17/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "feedDetailViewController.h"

@interface feedDetailViewController ()

@end

@implementation feedDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    apd = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    //notify this page to load more images
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadImage) name:@"loadMore" object:nil];

    _img.layer.cornerRadius = _img.frame.size.width/2;
    _img.clipsToBounds=YES;
    [self loadImage];
   
}
-(void)loadImage
{
    
    UIImage* img =[[apd feedImages]objectForKey:[[apd currentFeedItem] objectForKey:@"img"]];
    
    if (img) {
        [_img setImage:img];
    }
    else
    {
        [_img setImage:nil];
        UIActivityIndicatorView* loading = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(_img.frame.size.width/2 - 35, _img.frame.size.height/2 - 35, 70, 70)];
        [loading setColor:[UIColor blackColor]];
        [loading startAnimating];
        [_img addSubview:loading];
        [apd getImagesInBackground:[[apd currentFeedItem] objectForKey:@"img"]];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
@end
