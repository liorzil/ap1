//
//  loginViewController.h
//  Blank Project
//
//  Created by Lee Silver on 3/10/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
//@interface loginViewController : PFLogInViewController <UIGestureRecognizerDelegate, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@interface loginViewController : UIViewController <UIGestureRecognizerDelegate, FBLoginViewDelegate>
{
    AppDelegate* apd;
}

@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtName;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
- (IBAction)btnSignup:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnTwitter;
- (IBAction)btnTwitter_click:(id)sender;
- (IBAction)btnFacebook:(id)sender;
- (IBAction)taptap:(id)sender;
@property (weak, nonatomic) IBOutlet FBLoginView *loginView;
@property (weak, nonatomic) IBOutlet UIButton *btnLoginButton;
- (IBAction)btnLogin:(id)sender;

@end
