//
//  photoViewController.m
//  EVNTAPP
//
//  Created by Lee Silver on 2014-06-09.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "photoViewController.h"

@interface photoViewController ()

@end

@implementation photoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(![self.slidingViewController.underLeftViewController isKindOfClass:[menuViewController class]])
    {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
        
    apd = (AppDelegate*)[[UIApplication sharedApplication]delegate];

//    NSDictionary* elements = [[NSDictionary alloc]initWithDictionary:[apd.beamInfo objectForKey:@"elements"]];
//    
//    NSLog(@"%@, %@", elements, [elements objectForKey:@"content"]);
    
    [_img setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[apd.beamInfo objectForKey:@"content"]]]]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnClose:(id)sender {
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoFeed" object:nil];
}
@end
