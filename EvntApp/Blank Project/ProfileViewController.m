//
//  ProfileViewController.m
//  Blank Project
//
//  Created by Lee Silver on 3/17/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "ProfileViewController.h"
#import "feed.h"
#import "UIImage+StackBlur.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    apd = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSLog(@"Current user info - %@", apd.currentUser);
    
    //round image
    NSURL *imageURL = [NSURL URLWithString:[apd.currentUser objectForKey:@"img"]];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    _profileImage.image = [UIImage imageWithData:imageData];
    _profileImage.layer.cornerRadius = _profileImage.frame.size.width/2;
    _profileImage.clipsToBounds = YES;
    [_profileImage.layer setBorderColor: [[UIColor whiteColor] CGColor]];
    [_profileImage.layer setBorderWidth: 0.7];
    
    _backgroundImage.image = [UIImage imageWithData:imageData];
    UIImage *newIma = [_backgroundImage.image stackBlur:13];
    _backgroundImage.image = newIma;
    
    _nameLabel.text = [apd.currentUser objectForKey:@"name"];
    
    //sliding menu
    if(![self.slidingViewController.underLeftViewController isKindOfClass:[menuViewController class]])
    {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    _profileMenu = @[@"Name", @"Email", @"Birth Date", @"Gender"];
    _profileValues = [[NSMutableArray alloc] initWithArray:@[[apd.currentUser objectForKey:@"email"], [apd.currentUser objectForKey:@"name"], [apd.currentUser objectForKey:@"facebookID"]]];
    
    NSLog(@"profile values%@", _profileValues);
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)btnChangeImage:(id)sender {
    
    [self camera:self];
}

- (IBAction)btnMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];
    
}
#pragma mark Table

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_profileMenu count];
}


-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"YOUR INFORMATION";
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        
    }
    [[cell textLabel] setTextColor:[UIColor blackColor]];
    [[cell textLabel] setFont:[UIFont fontWithName:@"Comfortaa" size:16]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    
    if ([[_profileMenu objectAtIndex:indexPath.row ] isEqualToString:@"Name"]) {
        [[cell textLabel]setText:[_profileMenu objectAtIndex:indexPath.row]];
        [[cell detailTextLabel] setText:[[apd currentUser] objectForKey:@"name"]];
        return cell;
    }
    else if ([[_profileMenu objectAtIndex:indexPath.row ] isEqualToString:@"Email"]) {
        [[cell textLabel]setText:[_profileMenu objectAtIndex:indexPath.row]];
        [[cell detailTextLabel] setText:[[apd currentUser] objectForKey:@"email"]];
        return cell;
    }
    else    if ([[_profileMenu objectAtIndex:indexPath.row ] isEqualToString:@"Birth Date"]) {
        [[cell textLabel]setText:[_profileMenu objectAtIndex:indexPath.row]];
        [[cell detailTextLabel] setText:[[apd currentUser] objectForKey:@"age"]];
        return cell;
    }    else     if ([[_profileMenu objectAtIndex:indexPath.row ] isEqualToString:@"Gender"]) {
        [[cell textLabel]setText:[_profileMenu objectAtIndex:indexPath.row]];
        [[cell detailTextLabel] setText:[[apd currentUser] objectForKey:@"gender"]];
        return cell;
    }
    else
    {
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    apd.ProfileEditItem = [[tableView cellForRowAtIndexPath:indexPath]textLabel].text;
    
    //[tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self performSegueWithIdentifier: @"ProfileDetail" sender:self];
    
    
    
}

#pragma mark camera actions
- (IBAction)camera:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@""
                                  delegate:self
                                  cancelButtonTitle:@"Cancel"
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Camera", @"Camera Roll", nil];
    
    
    [actionSheet showInView:self.view];
}



- (void) useCamera
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = NO;
        
        [self presentViewController:imagePicker animated:YES completion:^{
            //
        }];
        
        newMedia = YES;
        
    }
}

- (void) useCameraRoll
{
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.mediaTypes = [NSArray arrayWithObjects:
                                  (NSString *) kUTTypeImage,
                                  nil];
        imagePicker.allowsEditing = NO;
        
        
        [self presentViewController:imagePicker animated:YES completion:^{
            //
        }];
        
        newMedia = NO;
    }
}
-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = [info
                           objectForKey:UIImagePickerControllerMediaType];
    
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = [info
                          objectForKey:UIImagePickerControllerOriginalImage];
        
//        AppDelegate *appdel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//        
//        appdel.imageToCrop= image;
//
        [picker dismissViewControllerAnimated:YES completion:^{
            
        }];
//        [self dismissViewControllerAnimated:YES completion:^{
            //
//            [self performSegueWithIdentifier:@"gotoCrop" sender:self];
//        }];
        return;
        
//        if (newMedia)
//        {
//            UIImageWriteToSavedPhotosAlbum(image,
//                                           self,
//                                           @selector(image:finishedSavingWithError:contextInfo:),
//                                           nil);
//        }
//        else
//        {
//            [self dismissViewControllerAnimated:YES completion:^{
//                //
//                AppDelegate *appdel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//                
//                appdel.imageToCrop= image;
//                [self performSegueWithIdentifier:@"gotoCrop" sender:self];
//            }];
//        }
    }
    else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie])
    {
		// Code here to support video if enabled
	}
}

-(void)image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Error"
                              message: @"Failed to save image"\
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        if(image)
        {
//            [self dismissViewControllerAnimated:YES completion:^{
//
//                AppDelegate *appdel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//                
//                appdel.imageToCrop= image;
//                [self performSegueWithIdentifier:@"gotoCrop" sender:self];
//            }];
    
        }
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:^{
        
    }];
//    [self dismissViewControllerAnimated:YES completion:^{
//        
//    }];
    
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 0) {
        [self useCamera];
    }
    else if (buttonIndex == 1)
    {
        [self useCameraRoll];
    }
    return;
    
}

@end
