//
//  ProfileDetailViewController.h
//  EVNTAPP
//
//  Created by Orin on 23/06/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "menuViewController.h"
#import "AppDelegate.h"
#import "slidingPageViewController.h"
#import "ProfileViewController.h"

@interface ProfileDetailViewController : UIViewController
{
    AppDelegate* apd;
}
- (IBAction)btnReturn:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImg;
@property (weak, nonatomic) IBOutlet UILabel *txtDescription;
- (IBAction)tap:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *txt;
@property (weak, nonatomic) IBOutlet UIDatePicker *bday;
@property (weak, nonatomic) IBOutlet UISegmentedControl *maleFemail;

@end
