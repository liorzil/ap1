//
//  settingsViewController.m
//  EVNTAPP
//
//  Created by Orin on 18/06/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "settingsViewController.h"

@interface settingsViewController ()

@end

@implementation settingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //sliding menu
    if(![self.slidingViewController.underLeftViewController isKindOfClass:[menuViewController class]])
    {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    apd = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    if ([[apd.currentUser objectForKey:@"privacyPoke"]integerValue] == 1) {
        [_swtchPrivacy setOn:YES];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];

}

//change pin
- (IBAction)swtchPrivateChange:(id)sender {
    
    NSNumber* onOff;
    
    if ([_swtchPrivacy isOn]) {
        [onOff initWithInt:1];
    }
    else{
        [onOff initWithInt:0];
    }
    
    [apiCall setPrivacy:[NSNumber numberWithInt:1] forUser:[[NSUserDefaults standardUserDefaults]objectForKey:@"userID"] resultSuccess:^(NSDictionary *res) {
        NSLog(@"%@", res);
    } resultFailed:^(NSString *err) {
        NSLog(@"%@", err);
    }];
}




@end
