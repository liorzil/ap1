//
//  contactCell.h
//
//  Created by Lee Silver on 2013-12-17.
//
//Include this class and reference it to create uitableviewCells with fixed image size and aligned text

/////////////////////////// Include the following code in cellForRowAtIndexPath///////////////////////////////////////
//
//    if (cell == nil) {
//        cell = [[SizableImageCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
//    }
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#import <UIKit/UIKit.h>


@interface SizableImageCell : UITableViewCell {}
@end

