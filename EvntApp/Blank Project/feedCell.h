//
//  feedCell.h
//  Blank Project
//
//  Created by Lee Silver on 3/10/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface feedCell : UITableViewCell




@property (strong, nonatomic) IBOutlet UILabel *EVNT_Title;
@property (strong, nonatomic) IBOutlet UILabel *EVNT_Content;
@property (strong, nonatomic) IBOutlet UIImageView *EVNT_Img;
@property (strong, nonatomic) IBOutlet UILabel *EVNT_Location;

@property (strong, nonatomic) IBOutlet UILabel *EVNT_postedTime;
@property (strong, nonatomic) IBOutlet UIImageView *feedTypeImg;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet feedCell *bgView;
@end
