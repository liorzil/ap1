//
//  apiCall.h
//  EVNTAPP
//
//  Created by Lee Silver on 2014-03-26.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface apiCall : NSObject

+(void)getFeed:(void(^)(NSArray *res))success resultFailed:(void(^)(NSString *err))failed;
+(void)logActivity:(NSString*)activity forUser:(NSString*)username andBeacon:(NSString*)beaconID resultSuccess:(void(^)(NSDictionary *res))success resultFailed:(void(^)(NSString *err))failed;
+(void)getBeacons:(void(^)(NSArray *res))success resultFailed:(void(^)(NSString *err))failed;
+(void)getParticipants:(void(^)(NSArray *res))success resultFailed:(void(^)(NSString *err))failed;
+(void)loginWithParameters:(NSDictionary*)params resultSuccess:(void(^)(NSDictionary *res))success resultFailed:(void(^)(NSString *err))failed;
+(void)setPrivacy:(NSNumber*)allowPing forUser:(NSString*)username resultSuccess:(void(^)(NSDictionary *res))success resultFailed:(void(^)(NSString *err))failed;

@end
