//
//  LoadingView.h
//
//
//  Created by Lee Silver on 13-12-19.
//
///////////
//
//       Declare in .h
//           LoadingView* wait;
//
//      initialize in ViewDidLoad:
//           wait = [[LoadingView alloc]initWithFrame:webPage.frame];
//           [[self view]addSubview:wait];
//           [self bringSubviewToFront:wait]
//           [wait hide];
//
//      usage: [wait show], [wait hide]


#import <UIKit/UIKit.h>

@interface LoadingView : UIView
{
}
-(void)show;
-(void)hide;
@end
