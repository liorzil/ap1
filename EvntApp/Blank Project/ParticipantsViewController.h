//
//  ParticipantsViewController.h
//  Blank Project
//
//  Created by Lee Silver on 3/17/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "menuViewController.h"
#import "contactCell.h"
#import "AppDelegate.h"
@interface ParticipantsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
   // NSMutableArray* participants;
    AppDelegate* apd;
}
- (IBAction)btnMenu:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@end
