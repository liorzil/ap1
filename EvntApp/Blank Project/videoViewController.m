//
//  videoViewController.m
//  EVNTAPP
//
//  Created by Lee Silver on 2014-06-03.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "videoViewController.h"

@interface videoViewController ()

@end

@implementation videoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(![self.slidingViewController.underLeftViewController isKindOfClass:[menuViewController class]])
    {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    apd = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//    
//    NSDictionary* elements = [[NSDictionary alloc]initWithDictionary:[apd.beamInfo objectForKey:@"elements"]];
//    
//    NSLog(@"%@", elements);
//    
    moviePlayerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:[apd.beamInfo objectForKey:@"content"]]];

//    MPMoviePlayerViewController* moviePlayerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:@"https://d2pq0u4uni88oo.cloudfront.net/projects/68097/video-75138-h264_high.mp4"]];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:moviePlayerViewController.moviePlayer];
    //                MPMoviePlayerController *moviePlayer=[[MPMoviePlayerController alloc] initWithContentURL:url];
    
    
    moviePlayerViewController.moviePlayer.controlStyle=MPMovieControlStyleDefault;
    moviePlayerViewController.moviePlayer.shouldAutoplay=YES;
    [moviePlayerViewController.moviePlayer setFullscreen:YES animated:YES];
    UIButton* X = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    [X setTitle:@"X" forState:normal];
    [[moviePlayerViewController view]addSubview:X];
    [X addTarget:self action:@selector(closeWindow:) forControlEvents:UIControlEventTouchUpInside];
    
    [self presentMoviePlayerViewControllerAnimated:moviePlayerViewController];
    
//    [self.view addSubview:moviePlayerViewController.view];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void) moviePlayBackDidFinish:(NSNotification*)notification
{
    
    MPMoviePlayerController *player = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification object:player];
    
    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
    {
        [player.view removeFromSuperview];
    }
    
}

-(void) closeWindow:(id)sender
{
    [self dismissMoviePlayerViewControllerAnimated];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"gotoFeed" object:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
