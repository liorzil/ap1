//
//  loginViewController.m
//  Blank Project
//
//  Created by Lee Silver on 3/10/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "loginViewController.h"

@interface loginViewController ()

@end

@implementation loginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    apd = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	// Do any additional setup after loading the view.
    [super viewDidLoad];
    
    self.loginView.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    self.loginView.delegate = self;
    
    
    [_txtName setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [_txtEmail setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [_txtPassword setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    _txtName.layer.cornerRadius = 5;
    _txtEmail.layer.cornerRadius = 5;
    _txtPassword.layer.cornerRadius = 5;
    _btnLoginButton.layer.cornerRadius = 5;

//    [self.logInView.facebookButton setImage:nil forState:UIControlStateNormal];
//    [self.logInView.facebookButton setImage:nil forState:UIControlStateHighlighted];
//    [self.logInView.facebookButton setBackgroundImage:[UIImage imageNamed:@"fb.png"] forState:UIControlStateNormal];
//    [self.logInView.facebookButton setTitle:@"" forState:UIControlStateNormal];
//    [self.logInView.facebookButton setTitle:@"" forState:UIControlStateHighlighted];
//    
//    [self.logInView.twitterButton setImage:nil forState:UIControlStateNormal];
//    [self.logInView.twitterButton setImage:nil forState:UIControlStateHighlighted];
//    [self.logInView.twitterButton setBackgroundImage:[UIImage imageNamed:@"twitter.png"] forState:UIControlStateHighlighted];
//    [self.logInView.twitterButton setTitle:@"" forState:UIControlStateNormal];
//    [self.logInView.twitterButton setTitle:@"" forState:UIControlStateHighlighted];
//    
//    [self.logInView.signUpButton setTitle:@"Sign Up" forState:UIControlStateNormal];
//    [self.logInView.logInButton setTitle:@"Login" forState:normal];
//    
//    // Remove text shadow
//    CALayer *layer = self.logInView.usernameField.layer;
//    layer.shadowOpacity = 0.0;
//    layer = self.logInView.passwordField.layer;
//    layer.shadowOpacity = 0.0;
//    
//    // Set field text color
//    [self.logInView.usernameField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
//    [self.logInView.passwordField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
//    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnSignup:(id)sender {
    apd.loggedIn = YES;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (IBAction)btnTwitter_click:(id)sender {
    apd.loggedIn = YES;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)btnFacebook:(id)sender {
    apd.loggedIn = YES;
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)taptap:(id)sender {
    [_txtEmail resignFirstResponder];
    [_txtName resignFirstResponder];
    [_txtPassword resignFirstResponder];

}

#pragma Mark Parse Login Delegates
// Sent to the delegate to determine whether the log in request should be submitted to the server.
//- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
//    // Check if both fields are completed
//    if (username && password && username.length != 0 && password.length != 0) {
//        return YES; // Begin login process
//    }
//    
//    [[[UIAlertView alloc] initWithTitle:@"Missing Information"
//                                message:@"Make sure you fill out all of the information!"
//                               delegate:nil
//                      cancelButtonTitle:@"ok"
//                      otherButtonTitles:nil] show];
//    return NO; // Interrupt login process
//}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // Set frame for elements
//    [self.logInView.facebookButton setFrame:CGRectMake(35.0f, 287.0f, 120.0f, 40.0f)];
//    [self.logInView.twitterButton setFrame:CGRectMake(35.0f+130.0f, 287.0f, 120.0f, 40.0f)];
//    [self.logInView.signUpButton setFrame:CGRectMake(35.0f, 385.0f, 250.0f, 40.0f)];
//    [self.logInView.usernameField setFrame:CGRectMake(35.0f, 145.0f, 250.0f, 50.0f)];
//    [self.logInView.passwordField setFrame:CGRectMake(35.0f, 195.0f, 250.0f, 50.0f)];
}

#pragma mark Facebook
// This method will be called when the user information has been fetched
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
//    self.profilePictureView.profileID = user.id;
//    self.nameLabel.text = user.name;
    NSLog(@"%@", user);
    
    NSDictionary* userInfo = [[NSDictionary alloc]initWithObjectsAndKeys:
                              [user objectForKey:@"name"], @"user_name",
                              [user objectForKey:@"email"], @"user_email",
                              [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large", user.objectID],@"user_img",
                              [user objectForKey:@"gender"],@"user_gender",
                              @"iOS",@"platfom",
                              user.objectID, @"facebookID",
                              [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceID"],@"user_regID",
                              [user objectForKey:@"birthday"],@"user_age",nil];
    
    NSLog(@"%@", userInfo);
    
    [apiCall loginWithParameters:userInfo resultSuccess:^(NSDictionary *res) {
        
        NSLog(@"%@", res);
        [[NSUserDefaults standardUserDefaults] setObject:[res objectForKey:@"_id"] forKey:@"userID"];
        apd.currentUser = [[NSMutableDictionary alloc]initWithDictionary:res];
        NSLog(@"Current user has the following details: %@", apd.currentUser);
        [self performSegueWithIdentifier:@"goOn" sender:self];
    } resultFailed:^(NSString *err) {
        NSLog(@"%@", err);
    }];
    
    
//    { "user_name": "Aaron Phillips”,
// 		"user_email": null,
//        "user_img": "http://graph.facebook.com/512705599/picture?type=large”,
//        "user_gender": "male”,
//        "user_age": "07/09/1981”,
//		"platfom": "iOS”,
//        "facebookID": "”,
//        "user_regID":"” }
    
}
// Logged-in user experience
- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    NSLog(@"You're logged in as");
}
// Logged-out user experience
- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
//    self.profilePictureView.profileID = nil;
//    self.nameLabel.text = @"";
    NSLog(@"You're not logged in!");
}
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error {
    NSString *alertMessage, *alertTitle;
    
    // If the user should perform an action outside of you app to recover,
    // the SDK will provide a message for the user, you just need to surface it.
    // This conveniently handles cases like Facebook password change or unverified Facebook accounts.
    if ([FBErrorUtility shouldNotifyUserForError:error]) {
        alertTitle = @"Facebook error";
        alertMessage = [FBErrorUtility userMessageForError:error];
        
        // This code will handle session closures that happen outside of the app
        // You can take a look at our error handling guide to know more about it
        // https://developers.facebook.com/docs/ios/errors
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession) {
        alertTitle = @"Session Error";
        alertMessage = @"Your current session is no longer valid. Please log in again.";
        
        // If the user has cancelled a login, we will do nothing.
        // You can also choose to show the user a message if cancelling login will result in
        // the user not being able to complete a task they had initiated in your app
        // (like accessing FB-stored information or posting to Facebook)
    } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
        NSLog(@"user cancelled login");
        
        // For simplicity, this sample handles other errors with a generic message
        // You can checkout our error handling guide for more detailed information
        // https://developers.facebook.com/docs/ios/errors
    } else {
        alertTitle  = @"Something went wrong";
        alertMessage = @"Please try again later.";
        NSLog(@"Unexpected error:%@", error);
    }
    
    if (alertMessage) {
        [[[UIAlertView alloc] initWithTitle:alertTitle
                                    message:alertMessage
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
}
- (IBAction)btnLogin:(id)sender {
    UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Oops!" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    
    //validate
    if (![mc validateEmail:_txtEmail.text]) {
        [alert setMessage:@"Please enter a valid email address"];
        [alert show];
        return;
    }
    if ([[_txtName text] length] <1) {
        [alert setMessage:@"Please enter your name"];
        [alert show];
        return;
    }
    if ([[_txtPassword text] length] <1) {
        [alert setMessage:@"Please enter your password"];
        [alert show];
        return;
    }
    
    NSDictionary* userInfo = [[NSDictionary alloc]initWithObjectsAndKeys:
                              _txtName.text, @"user_name",
                              _txtEmail.text, @"user_email",
                              @"",@"user_img",
                              @"",@"user_gender",
                              @"iOS",@"platfom",
                              @"", @"facebookID",
                              [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceID"],@"user_regID",
                              @"",@"user_age",nil];
    
    [apiCall loginWithParameters:userInfo resultSuccess:^(NSDictionary *res) {
        NSLog(@"%@", res);
        //save userID
        [[NSUserDefaults standardUserDefaults] setObject:[res objectForKey:@"_id"] forKey:@"userID"];
        apd.currentUser = [[NSMutableDictionary alloc]initWithDictionary:res];
       
        [self performSegueWithIdentifier:@"goOn" sender:self];

    } resultFailed:^(NSString *err) {
        NSLog(@"%@", err);
        [alert setMessage:err];
        
    }];
    
}
@end
