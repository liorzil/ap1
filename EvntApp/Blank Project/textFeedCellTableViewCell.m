//
//  textFeedCellTableViewCell.m
//  EVNTAPP
//
//  Created by Lee Silver on 2014-05-16.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "textFeedCellTableViewCell.h"

@implementation textFeedCellTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
