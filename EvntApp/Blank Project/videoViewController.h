//
//  videoViewController.h
//  EVNTAPP
//
//  Created by Lee Silver on 2014-06-03.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "menuViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface videoViewController : UIViewController
{
    AppDelegate* apd;
    MPMoviePlayerViewController* moviePlayerViewController;
}
@end
