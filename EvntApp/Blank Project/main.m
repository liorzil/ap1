//
//  main.m
//  Blank Project
//
//  Created by Lee Silver on 2013-12-16.
//  Copyright (c) 2013 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
