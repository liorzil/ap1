//
//  contactCell.m
//
//  Created by Lee Silver on 2013-12-17.
//

#import "contactCell.h"

@implementation SizableImageCell
- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    float desiredWidth = 50;
    self.imageView.frame = CGRectMake(0,0,desiredWidth,desiredWidth);
    [self.imageView setContentMode:UIViewContentModeScaleAspectFill];
    
//    self.imageView.layer.cornerRadius = self.imageView.frame.size.width/2;
//    self.imageView.clipsToBounds = YES;
    
    float w=self.imageView.frame.size.width;
    if (w>desiredWidth) {
        float widthSub = w - desiredWidth;
        //        self.imageView.frame = CGRectMake(self.imageView.frame.origin.x,self.imageView.frame.origin.y,desiredWidth,self.imageView.frame.size.height);
        self.textLabel.frame = CGRectMake(self.textLabel.frame.origin.x-widthSub,self.textLabel.frame.origin.y,self.textLabel.frame.size.width+widthSub,self.textLabel.frame.size.height);
        self.detailTextLabel.frame = CGRectMake(self.detailTextLabel.frame.origin.x-widthSub,self.detailTextLabel.frame.origin.y,self.detailTextLabel.frame.size.width+widthSub,self.detailTextLabel.frame.size.height);
        //        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    //    self.imageView.frame = CGRectMake(5,5,34,34);
    //    self.imageView.layer.borderColor = ourGray.CGColor;
    //    self.imageView.layer.borderWidth =1;
    
}
@end

