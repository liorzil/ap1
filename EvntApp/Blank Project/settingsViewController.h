//
//  settingsViewController.h
//  EVNTAPP
//
//  Created by Orin on 18/06/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "slidingPageViewController.h"
#import "loginViewController.h"
#import <FacebookSDK/FacebookSDK.h>

@interface settingsViewController : UIViewController
{
    AppDelegate* apd;
}
- (IBAction)btnMenu:(id)sender;
- (IBAction)swtchPrivateChange:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *swtchPrivacy;

@end
