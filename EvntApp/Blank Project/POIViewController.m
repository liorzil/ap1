//
//  POIViewController.m
//  Blank Project
//
//  Created by Lee Silver on 3/17/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "POIViewController.h"

@interface POIViewController ()

@end

@implementation POIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //sliding menu
    if(![self.slidingViewController.underLeftViewController isKindOfClass:[menuViewController class]])
    {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];

}
@end
