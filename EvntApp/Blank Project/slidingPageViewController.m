//
//  slidingPageViewController.m
//  Blank Project
//
//  Created by Lee Silver on 2014-01-29.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "slidingPageViewController.h"

@interface slidingPageViewController ()

@end

@implementation slidingPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //set up sliding menu
    self.view.layer.shadowOpacity = 0.75f;
    self.view.layer.shadowRadius = 5.0f;
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    
    if(![self.slidingViewController.underLeftViewController isKindOfClass:[menuViewController class]])
    {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];

    
   
//    
    AppDelegate *apd = [[UIApplication sharedApplication]delegate];
//    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(10,120, 300, 500)];
//    [lbl setNumberOfLines:0];
//    [lbl setLineBreakMode:NSLineBreakByWordWrapping];
//    [lbl setText:[NSString stringWithFormat:@"%@",apd.pageInfo]];
//    
    [_lblPageTitle setText:[NSString stringWithFormat:@"%@", apd.pageInfo]];
//
//    [self.view addSubview:lbl];
//    UIView* feedView = [[feed alloc]initWithFrame:self.view.frame];
//    [self.view addSubview:feedView];
//    [[self view]bringSubviewToFront:feedView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];    
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnMenuClicked:(id)sender {
    
    [self.slidingViewController anchorTopViewTo:ECRight];

}
@end
