//
//  testViewController.h
//  EVNTAPP
//
//  Created by Lee Silver on 2014-05-21.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface testViewController : UIViewController
{
    AppDelegate* apd;
}
- (IBAction)btnConnect:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblOutput;
- (IBAction)btnWend:(id)sender;
- (IBAction)btnPing:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *txt;
- (IBAction)tap:(id)sender;
@end
