//
//  participantDetailsViewController.h
//  Blank Project
//
//  Created by Lee Silver on 3/17/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface participantDetailsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    AppDelegate* apd;
}
@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UILabel *lblCompanyName;
@property (strong, nonatomic) IBOutlet UITableView *tbl;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
- (IBAction)btnBack:(id)sender;
- (IBAction)ping:(id)sender;
@end
