//
//  InitViewController.m
//
//  Created by Lee Silver 1/29/2014

#import "InitViewController.h"
#import "AppDelegate.h"

@interface InitViewController ()

@end

@implementation InitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    apd = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    NSLog(@"Location Authorization status = %u", [CLLocationManager authorizationStatus]);
    
    self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Crowd"];
    

   //    [apd refreshNotifications];
  //  [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshNotificationCount" object:self userInfo:nil];

//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(gotoVideo) name:@"gotoVideo" object:nil];

}

//-(void)gotoVideo
//{
//    self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"video"];
//
//}
-(void)viewDidAppear:(BOOL)animated
{
    
    //bypass login
//    if (self.topViewController == Nil) {
//        self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Feed"];
//    }
    
    ///////////////////////////////////////////////
//   [self performSegueWithIdentifier:@"gotoTest" sender:self];
//    return;
    //////////////////////////////////////////////
    
    
    // Check if user is logged in
//    if (![PFUser currentUser]) {
//        // Instantiate our custom log in view controller
//        loginViewController *logInViewController = [[loginViewController alloc] init];
//        [logInViewController setDelegate:self];
//        [logInViewController setFacebookPermissions:[NSArray arrayWithObjects:@"friends_about_me", nil]];
//        [logInViewController setFields:PFLogInFieldsUsernameAndPassword
//         | PFLogInFieldsTwitter
//         | PFLogInFieldsFacebook
//         | PFLogInFieldsSignUpButton];
//        
//        // Instantiate our custom sign up view controller
//        PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc] init];
//        [signUpViewController setDelegate:self];
//        [signUpViewController setFields:PFSignUpFieldsDefault | PFSignUpFieldsAdditional];
//        
//        // Link the sign up view controller
//        [logInViewController setSignUpController:signUpViewController];
//        
        // Present log in view controller
//        [self presentViewController:logInViewController animated:YES completion:NULL];
//    }
//    else
//    {
//        if (self.topViewController == Nil) {
//            self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Participants"];
//        }
//    }
    

//    if ([apd loggedIn]>0) {
//        NSLog(@"%@", self.topViewController);
//        if (self.topViewController == Nil) {
//            self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Participants"];
//        }
//    }
//    else{
//        [self performSegueWithIdentifier:@"gotoLogin" sender:self];
//        
//    }

//    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
//        self.topViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"firstTime"];
//        
//    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
