//
//  textFeedCellTableViewCell.h
//  EVNTAPP
//
//  Created by Lee Silver on 2014-05-16.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface textFeedCellTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UIImageView *feedTypeImage;
@property (strong, nonatomic) IBOutlet UILabel *EVNT_Title;
@property (strong, nonatomic) IBOutlet UILabel *EVNT_Content;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;

@end
