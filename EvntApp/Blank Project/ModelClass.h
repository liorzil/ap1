//
//  modelClass.h

//  Created by Lee Silver on 2013-10-24.
//  Copyright (c) 2013 Lee Silver. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeychainItemWrapper.h"
//#import <Parse/Parse.h>

#define api_domain @"http://69.172.254.167/api/v1/"
//#define api_domain @"http://69.165.170.226/api/v1/"

#define apiKey [self getToken]
#define secret @"32BitCanBeUsedToCreatePassHash."



@interface ModelClass : NSObject<NSStreamDelegate>
{
}

+ (ModelClass *) sharedModel;
-(void)saveToken:(id)accessToken;
- (id)getToken;
- (BOOL) validateEmail: (NSString *) candidate;
-(BOOL) validatePhone:(NSString*)phoneNumber;
-(BOOL)isNumeric:(NSString*)text;
-(NSString*)niceDate:(NSString*)dateToFormat;


//generic API calls

/**
 * sendRequestWithMethod
 *  (get a list)
 *
 * Sends an API call with a method name, and parameters in the url
 * Success block returning an array of items
 * Lee Silver
 * March, 23 2014
 *
 */
-(void)sendRequestWithMethod:(NSString*)method AndParameters:(NSDictionary*)params resultSuccess:(void (^)(NSArray *))success resultFailed:(void (^)(NSString *))failed;

/**
 * sendRequestWithMethod
 *  (get 1 item)
 *
 * Sends an API call with a method name, and json parameters
 * Success block returning an NSDictionary item
 * Lee Silver
 * March, 23 2014
 *
 */
-(void)sendRequestWithMethod:(NSString*)method AndJSONParameters:(NSDictionary*)params resultSuccess:(void(^)(NSDictionary *))success resultFailed:(void(^)(NSString*))failed;


/**
 * getArrayBySendingRequestWithMethod
 *  (get a list)
 *
 * Sends an API call with a method name, and JSON parameters
 * Success block returning an array of items
 * Lee Silver
 * March, 23 2014
 *
 */
-(void)getArrayBySendingRequestWithMethod:(NSString*)method AndJSONParameters:(NSDictionary*)params resultSuccess:(void(^)(NSArray *))success resultFailed:(void(^)(NSString*))failed;

/**
 * sendGETRequestWithMethod
 *
 * Sends a GET API call with a method name
 * Success block returning an array of items
 * Lee Silver
 * March, 23 2014
 *
 */
-(void)sendGETRequestWithMethod:(NSString*)method resultSuccess:(void(^)(NSArray *))success resultFailed:(void(^)(NSString*))failed;

/**
 * callParseCloudMethodInBackground
 *
 * Calls a Parse Cloud Function
 * Success block returning an NSDIctionary of items
 * Lee Silver
 * April 6, 2014
 *
 */
-(void)callParseCloudMethodInBackground:(NSString*)methodName andParameters:(NSDictionary*)parameters resultSuccess:(void(^)(NSDictionary *))success resultFailed:(void(^)(NSString*))failed;

/**
 * callParseCloudMethod
 *
 * Calls a Parse Cloud Function that doesn't return anything
 * Lee Silver
 * April 6, 2014
 *
 */
-(void)callParseCloudMethod:(NSString*)methodName andParameters:(NSDictionary*)parameters;

@end
