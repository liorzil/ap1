#import "LoadingView.h"


@implementation LoadingView


#define LABEL_WIDTH 80
#define LABEL_HEIGHT 20
#define SPINNER_SIZE 30
#define IMG_SIZE 150


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor blackColor]];
        
        self.alpha = .7f;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width-LABEL_WIDTH)/2+20,
                                                                   (self.bounds.size.height-LABEL_HEIGHT)/2,
                                                                   LABEL_WIDTH,
                                                                   LABEL_HEIGHT)];
        label.text = @"Loading…";
        label.center = self.center;
        [label setTextColor:[UIColor whiteColor]];
        [label setBackgroundColor:[UIColor clearColor]];
        [self setTag:33];

        UIActivityIndicatorView* spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        //        spinner.frame = CGRectMake(label.frame.origin.x - LABEL_HEIGHT - 5,
        //                                   label.frame.origin.y,
        //                                   LABEL_HEIGHT,
        //                                   LABEL_HEIGHT);
        
        spinner.frame = CGRectMake((self.bounds.size.width-SPINNER_SIZE)/2+7,
                                   label.frame.origin.y -30,
                                   LABEL_HEIGHT,
                                   LABEL_HEIGHT);
        [spinner startAnimating];
        [self addSubview: spinner];
        [self addSubview: label];
    }
    return self;

}
-(void)show
{
    [self setHidden:NO];
}
-(void)hide
{
    [self setHidden:YES];
}
@end
