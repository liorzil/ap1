//
//  slidingPageViewController.h
//  Blank Project
//
//  Created by Lee Silver on 2014-01-29.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "slidingPageViewController.h"
#import "menuViewController.h"
#import "feed.h"

@interface slidingPageViewController : UIViewController

- (IBAction)btnMenuClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblPageTitle;
@end
