//
//  MapsViewController.h
//  Blank Project
//
//  Created by Lee Silver on 3/17/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "menuViewController.h"
#import <MapKit/MapKit.h>


@interface MapsViewController : UIViewController

- (IBAction)btnMenu:(id)sender;
@end
