//
//  menuViewController.h
//
//  Created by Lee Silver on 13-07-22.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "AppDelegate.h"

@interface menuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>// UITableViewController
{
    AppDelegate* apd;
}
@property(strong, nonatomic)NSMutableArray* menu;
@property (strong, nonatomic) IBOutlet UITableView *tbl;

@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@end
