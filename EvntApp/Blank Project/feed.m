//
//  feed.m
//  Blank Project
//
//  Created by Lee Silver on 3/10/2014.
//  Copyright (c) 2014 Lee Silver. All rights reserved.
//

#import "feed.h"

@implementation feed

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    apd = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //notify this page to load more images
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadMore) name:@"loadMore" object:nil];

    
	// Do any additional setup after loading the view.
    if(![self.slidingViewController.underLeftViewController isKindOfClass:[menuViewController class]])
    {
        self.slidingViewController.underLeftViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Menu"];
    }
    [self.view addGestureRecognizer:self.slidingViewController.panGesture];
    
    //custom view
    UINib *nib = [UINib nibWithNibName:@"feedCell" bundle:nil];
    [[self tbl] registerNib:nib forCellReuseIdentifier:@"feedCell"];
    
    
    [_tbl setAllowsSelection:YES];
//    [apiCall getBeacons:^(NSArray *res) {
//        NSLog(@"%@", res);
//        apd.beacons = [[NSArray alloc]initWithArray:res];
//    } resultFailed:^(NSString *err) {
//        NSLog(@"%@", err);
//    }];
    
    
}
-(void)loadMore
{
    [_tbl reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    
//        [_lblTest setText:@"Here I am"];
//        
//    }
//    return self;
//}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
#pragma mark Table

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return apd.feed.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* item = [apd.feed objectAtIndex:indexPath.section];
    NSDictionary* element = [((NSArray*)[item objectForKey:@"elements"]) objectAtIndex:indexPath.row];
    NSString* type =[element objectForKey:@"type"];
    if ([type isEqualToString:@"image"] || [type isEqualToString:@"video"])
    {
        return 213;
    }
    else
    {
        return 77;
    }

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return 1;
    NSDictionary* item = [apd.feed objectAtIndex:section];
    return ((NSArray*)[item objectForKey:@"elements"]).count;

}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"";
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"feedCell";
    
    feedCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[feedCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];

        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
    	cell = (feedCell *)[nib objectAtIndex:indexPath.section * indexPath.row];
    }

//    [cell bgView].layer.borderWidth = 2;
//    [cell bgView].layer.borderColor = [[UIColor blueColor]CGColor];
//    [cell bgView].layer.cornerRadius = 10;
//    [cell bgView].layer.masksToBounds = YES;
    
    NSDictionary* item = [apd.feed objectAtIndex:indexPath.section];
    NSLog(@"%@", item);
    NSDictionary* element = [((NSArray*)[item objectForKey:@"elements"]) objectAtIndex:indexPath.row];
    
    BOOL shortCell;
    if ([[element objectForKey:@"type"]isEqualToString:@"image"] || [[element objectForKey:@"type"]isEqualToString:@"video"]) {
        shortCell = false;
        [[cell EVNT_Img] setHidden:NO];
        [[cell lblUserName] setHidden:NO];
        [[cell userImage] setHidden:NO];
    }
    else
    {
        shortCell = true;
        [[cell EVNT_Img] setHidden:YES];
        [[cell lblUserName] setHidden:YES];
        [[cell userImage] setHidden:YES];
    }
    
//    if (!shortCell) {
        [[cell EVNT_postedTime]setText:[mc niceDate:[item objectForKey:@"created_at"]]];
//    }

    @try {
        [cell userImage].layer.cornerRadius = cell.userImage.frame.size.height / 2;
        cell.userImage.layer.masksToBounds = YES;
        UIImage* userImg =[[apd feedImages]objectForKey:[item objectForKey:@"user_logo"]];
        
        if (userImg) {
            [[cell userImage]setImage:userImg];
        }
        else
        {
            [[cell userImage] setImage:nil];
            [apd getImagesInBackground:[item objectForKey:@"user_logo"]];
            
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    [cell.lblUserName setText:[item objectForKey:@"username"]];
    
    @try {
        [[cell EVNT_Title]setText:[item objectForKey:@"title"]];
    }    @catch (NSException *exception) {
        [[cell EVNT_Title]setText:@""];
    } @finally {}

    
//    @try {
//        [[cell EVNT_Content]setText:[item objectForKey:@"content"]];
//    } @catch (NSException *exception) {} @finally {}
    
    
    if ([[element objectForKey:@"type"] isEqualToString:@"image"]) {
        
        //icon
        [cell.feedTypeImg setImage:[UIImage imageNamed:@"camera1.png"]];
        
        //get image
        @try {
            
            UIImage* img =[[apd feedImages]objectForKey:[element objectForKey:@"content"]];
            
            if (img) {
                [[cell EVNT_Img]setImage:img];
            }
            else
            {
                [[cell EVNT_Img] setImage:nil];
                UIActivityIndicatorView* loading = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(70, 0, 70, 70)];
                [loading setColor:[UIColor blackColor]];
                [loading startAnimating];
                [[cell EVNT_Img] addSubview:loading];
                [apd getImagesInBackground:[element objectForKey:@"content"]];
                
            }
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception);
        }
        @finally {
            
        }

    }
    else if ([[element objectForKey:@"type"] isEqualToString:@"video"]) {
        
        //icon
        [cell.feedTypeImg setImage:[UIImage imageNamed:@"video1.png"]];

        //get preview image
        @try {
            
            UIImage* img =[[apd feedImages]objectForKey:[element objectForKey:@"screenshot"]];
            
            if (img) {
                [[cell EVNT_Img]setImage:img];
            }
            else
            {
                [[cell EVNT_Img] setImage:nil];
                UIActivityIndicatorView* loading = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(70, 0, 70, 70)];
                [loading setColor:[UIColor blackColor]];
                [loading startAnimating];
                [[cell EVNT_Img] addSubview:loading];
                [apd getImagesInBackground:[element objectForKey:@"screenshot"]];
                
            }
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception);
        }
        @finally {
            
        }
        
    }
    else if ([[element objectForKey:@"type"] isEqualToString:@"text"]) {
        
        //icon
        [cell.feedTypeImg setImage:[UIImage imageNamed:@"text1.png"]];
        
        
        [[cell EVNT_Content] setText:[element objectForKey:@"content"]];
        
    }
    else if ([[element objectForKey:@"type"] isEqualToString:@"link"]) {
        
        [cell.feedTypeImg setImage:[UIImage imageNamed:@"link1.png"]];
        [[cell EVNT_Content] setText:[element objectForKey:@"content"]];

        
    }
    else
    {
        [[cell EVNT_Img] setHidden:YES];
        [[cell EVNT_Content] setText:[element objectForKey:@"content"]];


    }

    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    apd.currentFeedItem = [apd.feed objectAtIndex:indexPath.section];
    
    NSDictionary* element = [[NSDictionary alloc]init];
    
    element = [[apd.currentFeedItem objectForKey:@"elements"] objectAtIndex:indexPath.row];
    apd.beamInfo = [[NSMutableDictionary alloc]initWithDictionary:element];

    if ([[element objectForKey:@"type"]isEqualToString:@"image"])
    {
        //[self performSegueWithIdentifier:@"gotoVideo" sender:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"gotoPhoto" object:self userInfo:nil];
    }
    else if([[element objectForKey:@"type"]isEqualToString:@"video"])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"gotoVideo" object:self userInfo:nil];
    }

    NSLog(@"Element: %@", element);
}

- (IBAction)btnMenu:(id)sender {
    [self.slidingViewController anchorTopViewTo:ECRight];

}
@end
